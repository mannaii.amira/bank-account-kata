package fr.sgcib.exercice.bankaccountkata.business;


import java.time.LocalDate;

import javax.money.MonetaryAmount;

import fr.sgcib.exercice.bankaccountkata.model.Client;
import fr.sgcib.exercice.bankaccountkata.model.Operation;

public class BankAccountService {
	
	public void saveMoney(Client client, MonetaryAmount moneyToSave) {
		MonetaryAmount currentAmount = client.getSavingAccount()
				.getAmmount();
		MonetaryAmount newAmount = currentAmount.add(moneyToSave);
		client.getSavingAccount().setAmmount(newAmount);
		Operation operation = new Operation("Deposit"
				, LocalDate.now(), moneyToSave, newAmount);
		client.getOperations().add(operation);
	}
	
	public void retrieveMoney(Client client, MonetaryAmount moneyToGet) {
		MonetaryAmount currentAmount = client.getSavingAccount().getAmmount();
		MonetaryAmount newAmount = currentAmount.subtract(moneyToGet);
		client.getSavingAccount().setAmmount(newAmount);
		Operation operation = new Operation("Withdrawl"
				, LocalDate.now(), moneyToGet, newAmount);
		client.getOperations().add(operation);
	}
}
