package fr.sgcib.exercice.bankaccountkata.model;

import javax.money.MonetaryAmount;

public class BankAccount {
	MonetaryAmount balance;
	
	public BankAccount(MonetaryAmount ammount) {
		this.balance = ammount;
	}

	public MonetaryAmount getAmmount() {
		return balance;
	}

	public void setAmmount(MonetaryAmount ammount) {
		this.balance = ammount;
	}

}
