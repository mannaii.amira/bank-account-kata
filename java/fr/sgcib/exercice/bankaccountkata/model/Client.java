package fr.sgcib.exercice.bankaccountkata.model;

import java.util.ArrayList;
import java.util.List;

public class Client {
	private String name;
	private BankAccount savingAccount;
	private List<Operation> operations;
	
	public Client(String name, BankAccount savingAccount) {
		super();
		this.name = name;
		this.savingAccount = savingAccount;
		this.operations = new ArrayList<Operation>();
	}
	public Client(String name) {
		super();
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public BankAccount getSavingAccount() {
		return savingAccount;
	}
	public List<Operation> getOperations() {
		return operations;
	}
}
