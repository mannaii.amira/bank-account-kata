package fr.sgcib.exercice.bankaccountkata.model;

import java.time.LocalDate;

import javax.money.MonetaryAmount;

public class Operation {
	private String description;
	private LocalDate date;
	private MonetaryAmount operationAmount;
	private MonetaryAmount balance;

	public Operation(String description, LocalDate date
			, MonetaryAmount operationAmount
			, MonetaryAmount balance) {
		super();
		this.description = description;
		this.date = date;
		this.operationAmount = operationAmount;
		this.balance = balance;
	}

	public String getDescription() {
		return description;
	}

	public LocalDate getDate() {
		return date;
	}

	public MonetaryAmount getOperationAmount() {
		return operationAmount;
	}

	public MonetaryAmount getBalance() {
		return balance;
	}

	public boolean equals(Object object){
		if (object == null) {
			return false;
		}
		if(!(object instanceof Operation)) {
			return false;
		}
		Operation operation = (Operation) object;
		return description.equals(operation.getDescription()) 
				&& date.isEqual(operation.getDate())
				&& operationAmount.compareTo(operation.getOperationAmount()) == 0
				&& balance.compareTo(operation.getBalance()) == 0;
	}

	public int hashCode() {
		return operationAmount.getNumber().intValue() 
				* balance.getNumber().intValue();
	}

}
