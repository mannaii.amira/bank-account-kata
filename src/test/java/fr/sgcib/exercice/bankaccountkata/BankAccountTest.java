package fr.sgcib.exercice.bankaccountkata;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static java.util.Arrays.asList;

import java.util.List;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.junit.Assert;
import org.junit.Test;

import fr.sgcib.exercice.bankaccountkata.business.BankAccountService;
import fr.sgcib.exercice.bankaccountkata.model.BankAccount;
import fr.sgcib.exercice.bankaccountkata.model.Client;
import fr.sgcib.exercice.bankaccountkata.model.Operation;

public class BankAccountTest {

	BankAccountService bankAccountService = new BankAccountService();

	@Test
	public void makeDepositTest() {
		MonetaryAmount initialAccountAmount = Monetary.getDefaultAmountFactory().setNumber(1.1111).setCurrency("EUR").create();
		BankAccount account = new BankAccount(initialAccountAmount);
		Client client = new Client("Daniel", account);
		MonetaryAmount savedAmount = Monetary.getDefaultAmountFactory().setNumber(1.2522).setCurrency("EUR").create();		
		bankAccountService.saveMoney(client, savedAmount);
		assertThat(client.getSavingAccount().getAmmount().toString(), is("EUR 2.3633"));
	}
	
	@Test
	public void makeWithdrawlTest() {
		MonetaryAmount initialAccountAmount = Monetary.getDefaultAmountFactory().setNumber(1.5700).setCurrency("EUR").create();
		BankAccount account = new BankAccount(initialAccountAmount);
		Client client = new Client("Daniel", account);
		MonetaryAmount retrivedAmount = Monetary.getDefaultAmountFactory().setNumber(1.3000).setCurrency("EUR").create();		
		bankAccountService.retrieveMoney(client, retrivedAmount);
		assertThat(client.getSavingAccount().getAmmount().toString(), is("EUR 0.27"));
	}
	
	@Test
	public void getOperationHistoryTest() {
		MonetaryAmount initialAccountAmount = Monetary.getDefaultAmountFactory().setNumber(0).setCurrency("EUR").create();
		BankAccount account = new BankAccount(initialAccountAmount);
		Client client = new Client("Daniel", account);
		
		List<Operation> expected = createOperationList();
		makeOperations(client);
		Assert.assertArrayEquals(expected.toArray(), client.getOperations().toArray());
	}

	private List<Operation> createOperationList() {
		MonetaryAmount operationAmount = Monetary.getDefaultAmountFactory().setNumber(1.2500).setCurrency("EUR").create();
		MonetaryAmount balanceAmount2 = Monetary.getDefaultAmountFactory().setNumber(2.5000).setCurrency("EUR").create();
		MonetaryAmount balanceAmount3 = Monetary.getDefaultAmountFactory().setNumber(1.2000).setCurrency("EUR").create();
		MonetaryAmount operation3Amount  = Monetary.getDefaultAmountFactory().setNumber(1.3000).setCurrency("EUR").create();

		Operation operation1 = new Operation("Deposit", LocalDate.now(), operationAmount, operationAmount);
		Operation operation2 = new Operation("Deposit", LocalDate.now(), operationAmount, balanceAmount2);
		Operation operation3 = new Operation("Withdrawl", LocalDate.now(), operation3Amount, balanceAmount3);
		
		List<Operation> expected = asList(operation1, operation2, operation3);
		return expected;
	}
	
	private void makeOperations(Client client) {
		MonetaryAmount savedAmount = Monetary.getDefaultAmountFactory().setNumber(1.2500).setCurrency("EUR").create();		
		MonetaryAmount retrivedAmount = Monetary.getDefaultAmountFactory().setNumber(1.3000).setCurrency("EUR").create();		
		bankAccountService.saveMoney(client, savedAmount);
		bankAccountService.saveMoney(client, savedAmount);
		bankAccountService.retrieveMoney(client, retrivedAmount);
	}
}
